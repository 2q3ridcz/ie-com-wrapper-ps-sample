$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\..\" | Resolve-Path

$PackageName = "GooglePageIE"
$PackagePath = $ProjectFolderPath | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"
Import-Module -Name $PackagePath -Force

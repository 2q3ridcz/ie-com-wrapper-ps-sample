﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")

Describe "GoogleSearchPageIE" {
    BeforeEach {
        $IE = New-IEComWrapper
        $IE.SetVisible($True)
        $IE.Navigate("https://www.google.co.jp/search?q=test")
        $IE.Wait()
    }

    AfterEach {
        If ($IE) {
            $IE.Quit()
        }
    }

    It "Can run google search" {
        $GooglePage = Get-GoogleSearchPageIE -HWND $IE.IE.HWND
        $GooglePage.Search("time japan")
        $GooglePage.GetTitle() | Should BeLike "time japan - Google*"
    }

    It "Can save" {
        $testPath = "TestDrive:\test.txt"

        $GooglePage = Get-GoogleSearchPageIE -HWND $IE.IE.HWND
        $GooglePage.Save($testPath, "UTF8")
        (Get-Content -Path $testPath) -join "" | Should BeLike "<html*</html>"
}
}
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\" | Resolve-Path

$PackageName = "GooglePageIE"
$PackagePath = $ProjectFolderPath | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"
Import-Module -Name $PackagePath -Force


$OutputFolderPath = "$here\Output" | Resolve-Path
$OutputFilePath = $OutputFolderPath | Join-Path -ChildPath "html.txt"

Write-Host ("Start IE...")

$IE = New-IEComWrapper
$IE.SetVisible($Visible)
$IE.Navigate("https://www.google.co.jp/search?q=test")
$IE.Wait()

Write-Host ("Get GoogleIE...")

$GooglePage = Get-GoogleSearchPageIE -HWND $IE.IE.HWND

Write-Host ("Search...")
$GooglePage.Search("time japan")

Write-Host ("GetTitle...")
$Title = $GooglePage.GetTitle()

Write-Host ("Title: " + $Title)

$GooglePage.Save($OutputFilePath, "UTF8")

Read-Host ("Fin!") | Out-Null

$IE.Quit()


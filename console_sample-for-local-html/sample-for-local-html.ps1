$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\" | Resolve-Path

$PackageName = "GooglePageIE"
$PackagePath = $ProjectFolderPath | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"
Import-Module -Name $PackagePath -Force


$InputFolderPath = "$here\Input" | Resolve-Path
$OutputFolderPath = "$here\Output" | Resolve-Path

$OutputFilePath = $OutputFolderPath | Join-Path -ChildPath "html.txt"

$HtmlFilePath = $InputFolderPath | Join-Path -ChildPath "sample-for-local-html.html"
$Uri = [Uri]::new($HtmlFilePath).AbsoluteUri


Write-Host ("Start IE...")

$IE = New-IEComWrapper
$IE.SetVisible($Visible)
$IE.Navigate($Uri)
$IE.Wait()


Write-Host ("Get GoogleIE...")

$GooglePage = Get-GoogleSearchPageIE -HWND $IE.IE.HWND


Write-Host ("GetTitle...")

$Title = $GooglePage.GetTitle()
Write-Host ("Title: " + $Title)


Write-Host ("Save...")
$GooglePage.Save($OutputFilePath, "UTF8")

Read-Host ("Fin!") | Out-Null

$IE.Quit()

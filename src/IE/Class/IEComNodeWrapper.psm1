﻿class IEComNodeWrapper {
    [__ComObject] $Node

    IEComNodeWrapper([__ComObject] $Node) {
        $this.Node = $Node
    }

    [IEComNodeWrapper] GetParentNode() {
        return [IEComNodeWrapper]::new($this.Node.parentNode)
    }

    [Object] InvokeComMethod($MethodName, [array]$ArgumentList) {
        return [__ComObject].InvokeMember(
            $MethodName, [System.Reflection.BindingFlags]::InvokeMethod, $null, $this.Node, $ArgumentList
        )
    }

    [IEComNodeWrapper[]] InvokeComMethodAndGetIEComNodeWrapper($MethodName, [array]$ArgumentList) {
        return (
            $this.InvokeComMethod($MethodName, $ArgumentList) |
            ForEach-Object { [IEComNodeWrapper]::new($_) }
        )
    }
    
    [IEComNodeWrapper[]] QuerySelectorAll([string]$Selector) {
        $nodes = [System.__ComObject].InvokeMember("querySelectorAll", [System.Reflection.BindingFlags]::InvokeMethod, $null, $this.Node, $Selector)
        $result = New-Object System.Collections.Generic.List[IEComNodeWrapper]
        for ($i = 0; $i -lt $nodes.Length; $i++) {
            $result.Add([IEComNodeWrapper]::new(
                [System.__ComObject].InvokeMember("item", [System.Reflection.BindingFlags]::InvokeMethod, $null, $nodes, $i)
            ))
        }
        return $result
    }

    [IEComNodeWrapper] QuerySelector([string]$Selector) {
        return ($this.InvokeComMethodAndGetIEComNodeWrapper("querySelector", $Selector))[0]
    }
    
    [IEComNodeWrapper] GetElementById([string]$Id) {
        return ($this.InvokeComMethodAndGetIEComNodeWrapper("getElementById", $Id))[0]
    }

    [IEComNodeWrapper[]] GetElementsByName([string]$Name){
        return $this.InvokeComMethodAndGetIEComNodeWrapper("getElementsByName", $Name)
    }

    [IEComNodeWrapper[]] GetElementsByTagName([string]$TagName){
        return $this.InvokeComMethodAndGetIEComNodeWrapper("getElementsByTagName", $TagName)
    }

    [IEComNodeWrapper[]] GetElementsByClassName([string]$ClassName){
        return $this.InvokeComMethodAndGetIEComNodeWrapper("getElementsByClassName", $ClassName)
    }

    [bool] ScrollHorizontal() {
        $Before = $this.Node.scrollLeft
        $this.Node.scrollLeft = $this.Node.scrollLeft + $this.Node.clientWidth
        return -Not ($Before -Eq $this.Node.scrollLeft)
    }

    [bool] ScrollVertical() {
        $Before = $this.Node.scrollTop
        $this.Node.scrollTop = $this.Node.scrollTop + $this.Node.clientHeight
        return -Not ($Before -Eq $this.Node.scrollTop)
    }
}
﻿using module ..\Class\IEComElementWrapper.psm1

<#
.Synopsis
   Converts a com object to IEComElementWrapper object.
.DESCRIPTION
   Converts a com object to IEComElementWrapper object.
.EXAMPLE
   ConvertTo-IEComElementWrapper -Node ($IE.Document.getElementById("content"))
#>
function ConvertTo-IEComElementWrapper {
    [CmdletBinding()]
    [OutputType([IEComElementWrapper])]
    param (
        [Parameter(ValueFromPipeline=$True, Mandatory=$True)]
        [__ComObject]
        $Node
    )
    
    begin {
    }
    
    process {
        [IEComElementWrapper]::new($Node)
    }
    
    end {
    }
}

﻿using module ..\Class\IEComNodeWrapper.psm1

<#
.Synopsis
   Converts a com object to IEComNodeWrapper object.
.DESCRIPTION
   Converts a com object to IEComNodeWrapper object.
.EXAMPLE
   ConvertTo-IEComNodeWrapper -Node ($IE.Document.getElementById("content"))
#>
function ConvertTo-IEComNodeWrapper {
    [CmdletBinding()]
    [OutputType([IEComNodeWrapper])]
    param (
        [Parameter(ValueFromPipeline=$True, Mandatory=$True)]
        [__ComObject]
        $Node
    )
    
    begin {
    }
    
    process {
        [IEComNodeWrapper]::new($Node)
    }
    
    end {
    }
}

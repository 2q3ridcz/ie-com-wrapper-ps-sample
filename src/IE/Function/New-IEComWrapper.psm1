﻿using module ..\Class\IEComWrapper.psm1

<#
.Synopsis
   Creates an internet explorer com object.
.DESCRIPTION
   Reference: https://qiita.com/flasksrw/items/a1ff5fbbc3b660e01d96.
.EXAMPLE
   New-IEComWrapper -Visible $True
#>
function New-IEComWrapper {
    [CmdletBinding()]
    [OutputType([IEComWrapper])]
    param (
        [Parameter(Mandatory=$False)]
        [bool]
        $Visible = $True
    )
    
    begin {
    }
    
    process {
        $IE = [IEComWrapper]::new()
        $IE.SetVisible($Visible)
        $IE
    }
    
    end {
    }
}

﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\..\" | Resolve-Path

$PackageName = "IE"
$PackagePath = $ProjectFolderPath | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"
Import-Module -Name $PackagePath -Force


$FunctionFolderPath = "$here\Function" | Resolve-Path

Get-ChildItem -Path $FunctionFolderPath |
Where-Object { $_.Extension -Eq ".psm1" } |
ForEach-Object {
    Import-Module -Name $_.FullName -Force
}

Export-ModuleMember -Function "*"

﻿using module ..\Class\GoogleSearchPageIE.psm1

<#
.Synopsis
   Gets an object to handle google search page via internet explorer.
.DESCRIPTION
   Gets an object to handle google search page via internet explorer.
.EXAMPLE
   Get-GoogleSearchPageIE -LocationName "*Google*"
#>
function Get-GoogleSearchPageIE {
    [CmdletBinding()]
    [OutputType([GoogleSearchPageIE])]
    param (
        [Parameter(Mandatory=$False)]
        [String]
        $HWND = "*"
        ,
        [Parameter(Mandatory=$False)]
        [String]
        $LocationName = "*"
        ,
        [Parameter(Mandatory=$False)]
        [String]
        $LocationURL = "*"
    )
    
    begin {
    }
    
    process {
        Get-IEComWrapper -HWND $HWND -LocationName $LocationName -LocationURL $LocationURL |
        %{ [GoogleSearchPageIE]::new($_) }
    }
    
    end {
    }
}

﻿using module ..\..\IE\Class\IEComWrapper.psm1

class GoogleSearchPageIE {
    [IEComWrapper] $IEComWrapper

    GoogleSearchPageIE($IEComWrapper) {
        $this.IEComWrapper = $IEComWrapper
    }

    [string] GetTitle() {
        $IE = $this.IEComWrapper
        $Doc = $IE.GetDocumentElementWrapper()
        $resultElm = $Doc.QuerySelector("title")
        return $resultElm.Node.innerText
    }

    [void] Search([string]$SearchWord) {
        $IE = $this.IEComWrapper
        $Doc = $IE.GetDocumentElementWrapper()
        $searchBox = $Doc.QuerySelector("input.noHIxc")
        $searchBox.Node.Value = $SearchWord
        $Doc = $IE.GetDocumentElementWrapper()
        $btn = $Doc.GetElementById("qdClwb")
        $btn.Node.click()
        $IE.Wait()
    }

    [void] Save([string]$Path, [string]$Encoding) {
        $IE = $this.IEComWrapper
        $Doc = $IE.GetDocumentElementWrapper()
        $Doc.SaveHtmlFile($Path, $Encoding)
    }
}